import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import ContactMailIcon from "@material-ui/icons/ContactMail";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Map from "./GoogleMaps";
import { withScriptjs } from "react-google-maps";
import { withNamespaces } from "react-i18next";

// for the whole contact form error validation needs to be added (on future versions)
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(6),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function SignUp({ t, props }) {
  const MapLoader = withScriptjs(Map);
  const classes = useStyles();
  const [emailValue, setEmailValue] = useState(" ");

  const handleTextFieldChange = (e) => {
    //emailValue = e.target.value;
    setEmailValue(e.target.value);
    console.log(emailValue);
  };
  // error validation or empty textfield needs to be added
  const sendEmail = () => {
    const url = "http://127.0.0.1:5000/sendEmail/";
    const response = fetch(url + emailValue);
    console.log(response);
    alert("Email has been sent to " + emailValue);
  };
  const mapStyles = {
    width: "100%",
    height: "100%",
    alignItems: "center",
  };
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <ContactMailIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {t("contact.ContactUs")}
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label={t("contact.FirstName")}
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label={t("contact.LastName")}
                name="lastName"
                autoComplete="lname"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label={t("contact.Email")}
                name="email"
                autoComplete="email"
                onChange={handleTextFieldChange}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="address"
                label={t("contact.Address")}
                name="address"
                autoComplete="address"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="phone"
                label={t("contact.Phone")}
                name="phone"
                autoComplete="phone"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="description"
                label={t("contact.Description")}
                type="text"
                multiline
                id="description"
                autoComplete="description"
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label={t("contact.Promo")}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={sendEmail}
          >
            {t("contact.Send")}
          </Button>
          <Grid container justify="flex-end">
            <Grid item></Grid>
          </Grid>
        </form>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p>Contact:</p>
            <p>UTCN</p>
            <p>Strada George Baritiu 25-26</p>
            <p>Telefon 07232132</p>
          </div>
          <div class="col-sm-6"></div>
          <MapLoader
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAo9r-IfwxUw9NuHhJAPiAEK4NO01Z499c"
            loadingElement={<div style={{ height: `100%` }} />}
          />
        </div>
      </div>
    </Container>
  );
}
export default withNamespaces()(SignUp);
