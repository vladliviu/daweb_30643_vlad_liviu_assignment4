import React, { Component } from "react";
import "./News.css";
import newsdata from "../Resources/newsdata.xml";
import { withNamespaces } from "react-i18next";
import { useTranslation } from "react-i18next";
import CommentsSection from "./Comment";

class NewsPage extends React.Component {
  state = {
    NumberOfArticles: "",
    Title: "",
    Author: "",
    ImageURL: "",
    Description: "",
    AccessURL: "",
    newsData: [],
  };

  // se poate folosi un for pe index sau for each pentru a afisa toate cardurile.
  componentDidMount() {
    fetch(newsdata)
      .then((resp) => resp.text())
      .then(function (resp) {
        let parser = new DOMParser(),
          xmlDoc = parser.parseFromString(resp, "text/xml");
        return xmlDoc;
      })
      .then((xmlDocResponse) => {
        this.setState({
          Title: xmlDocResponse
            .getElementsByTagName("News")[0]
            .getElementsByTagName("NewsArticle")[0]
            .getElementsByTagName("Title")[0].childNodes[0].nodeValue,
          Author: xmlDocResponse
            .getElementsByTagName("News")[0]
            .getElementsByTagName("NewsArticle")[0]
            .getElementsByTagName("Author")[0].childNodes[0].nodeValue,
          ImageURL: xmlDocResponse
            .getElementsByTagName("News")[0]
            .getElementsByTagName("NewsArticle")[0]
            .getElementsByTagName("Image")[0].childNodes[0].nodeValue,
          Description: xmlDocResponse
            .getElementsByTagName("News")[0]
            .getElementsByTagName("NewsArticle")[0]
            .getElementsByTagName("Description")[0].childNodes[0].nodeValue,
          AccessURL: xmlDocResponse
            .getElementsByTagName("News")[0]
            .getElementsByTagName("NewsArticle")[0]
            .getElementsByTagName("AccessLink")[0].childNodes[0].nodeValue,
        });
      });
  }

  render() {
    const { t } = this.props;
    return (
      <div>
        <div className="container">
          <div className="card-deck row">
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="card">
                <div class="view overlay">
                  <img
                    class="card-img-top"
                    src={this.state.ImageURL}
                    alt="Card image cap"
                  />
                  <a href="#!">
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
                <div class="card-body">
                  <h4 class="card-title">
                    {this.state.Title} by {this.state.Author}
                  </h4>
                  <p class="card-text">{this.state.Description}</p>
                  <div>
                    <a
                      href={this.state.AccessURL}
                      class="btn btn-primary btn-sm"
                      target="_blank"
                    >
                      {t("about.More")}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <CommentsSection />
      </div>
    );
  }
}
export default withNamespaces()(NewsPage);
