﻿using System;
using System.Collections.Generic;

namespace Daw4.Models
{
    public partial class Users
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime? EmailVerifiedAt { get; set; }
        public string Password { get; set; }
        public string Hobbies { get; set; }
        public string Image { get; set; }
        public string RememberToken { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
