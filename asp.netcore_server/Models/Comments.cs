﻿using System;
using System.Collections.Generic;

namespace Daw4.Models
{
    public partial class Comments
    {
        public ulong Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
