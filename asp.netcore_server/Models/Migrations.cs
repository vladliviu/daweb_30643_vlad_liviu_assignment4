﻿using System;
using System.Collections.Generic;

namespace Daw4.Models
{
    public partial class Migrations
    {
        public uint Id { get; set; }
        public string Migration { get; set; }
        public int Batch { get; set; }
    }
}
