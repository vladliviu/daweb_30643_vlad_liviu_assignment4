import React, {Component, useEffect} from 'react'
import './App.css';
import Navbar from "./layout/navbar";
import NewsPage from "./pages/News";
import AboutPage from "./pages/About";
import ContactPage from "./pages/Contact";
import CoordinatorPage from "./pages/Coordinator";
import StudentProfilePage from "./pages/StudentProfile";
import LoginPage from "./pages/Login";
import RegisterPage from "./pages/Register";
import ProfilePage from "./pages/UserProfile";
import HomePage from "./pages/Home";
import ChartPage from "./pages/Charts";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { withNamespaces } from 'react-i18next';
import i18n from './i18n';
import { Button, ButtonGroup } from 'reactstrap';
import Cookies from "universal-cookie";
import { login, userProfile } from './components/apiRepo';

function App({t}) {

  const cookies = new Cookies();


  useEffect(() => {
    i18n.changeLanguage(cookies.get('lang'));
  }, [])
  
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
    cookies.set('lang', lng, {path: '/'});
  }
  return (
    <div>
    <Router>
      <Navbar/>
          <div className="col-sm-6 col-sm-offset-6">
      <ButtonGroup >
      <Button color="link" active onClick={() => changeLanguage('ro')}>  <i className="ro flag"></i></Button>
      <Button color="link" active onClick={() => changeLanguage('en')}>  <i className="uk flag"></i></Button>
    </ButtonGroup>
    </div>
      <Switch>
      <Route path="/" exact component={HomePage}/>
      <Route path="/news" component={NewsPage}/>
      <Route path="/about" component={AboutPage}/>
      <Route path="/studentprofile" component={StudentProfilePage}/>
      <Route path="/coordinator" component={CoordinatorPage}/>
      <Route path="/contact" component={ContactPage}/>
      <Route path="/login" component={LoginPage}/>
      <Route path="/register" component={RegisterPage}/>
      <Route path="/userprofile" component={ProfilePage}/>
      <Route path="/chart" component={ChartPage}/>
      </Switch>
    </Router>
    </div>

  );
}
export default withNamespaces()(App);
