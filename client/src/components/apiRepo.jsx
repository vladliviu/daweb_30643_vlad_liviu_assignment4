import axios from "axios";

export const register = (newUser) => {
  return axios
    .post("http://127.0.0.1:8000/api/register", newUser, {
      headers: { "Content-Type": "application/json" },
    })
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const login = (user) => {
  return axios
    .post(
      "http://127.0.0.1:8000/api/login",
      {
        email: user.email,
        password: user.password,
      },
      { headers: { "Content-Type": "application/json" } }
    )
    .then((res) => {
      localStorage.setItem("userToken", res.data.data.token);
      localStorage.setItem("userId", res.data.data.id);
      localStorage.setItem("username", res.data.data.name);
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getUser = () => {
  const url = "http://127.0.0.1:8000/api/users/" + localStorage.userId;
  //const url = urlraw + localStorage.userId;
  return axios
    .get(url, {
      headers: {
        Authorization: `Bearer ${localStorage.userToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

export const modifyUser = (newUser) => {
  const url =
    "http://127.0.0.1:8000/api/users/" + localStorage.userId + "?" + newUser;
  alert(url);
  return axios
    .put(url, {
      headers: {
        Authorization: `Bearer ${localStorage.userToken}`,
      },
    })
    .then((res) => {
      console.log("User modified");
      return res;
    })
    .catch((err) => {
      console.log(err);
      console.log(localStorage.userToken);
      alert(err);
    });
};

export const getComments = () => {
  const url = "http://127.0.0.1:8000/api/comments";
  //const url = urlraw + localStorage.userId;
  return axios
    .get(url, {
      headers: {
        Authorization: `Bearer ${localStorage.userToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};
