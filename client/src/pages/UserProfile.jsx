import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { withNamespaces } from "react-i18next";
import NativeSelect from "@material-ui/core/NativeSelect";
import InputBase from "@material-ui/core/InputBase";
import Typography from "@material-ui/core/Typography";
import { getUser, modifyUser } from "../components/apiRepo";
import axios from "axios";

// for the whole contact form error validation needs to be added (on future versions)
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(6),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function UserProfile({ t, url }) {
  const classes = useStyles();
  const [emailValue, setEmailValue] = useState("");
  const [passwordValue, setPasswordValue] = useState("");
  const [passwordcValue, setPasswordcValue] = useState("");
  const [usernameValue, setUsernameValue] = useState("");
  const [hobbyValue, setHobbyValue] = useState("");
  const [avatarValue, setAvatar] = useState("");

  const handleChangeProfile = () => {
    //http://127.0.0.1:8000/api/users/2?name=liviu1&email=vl@gmail.com&password=123456&hobbies=jogging&image=efg
    const newUser =
      "name=" +
      usernameValue +
      "&email=" +
      emailValue +
      "&password=" +
      passwordValue +
      "&hobbies=" +
      hobbyValue +
      "&image=" +
      avatarValue;

    alert(localStorage.getItem("userToken"));
    if (passwordValue === passwordcValue && passwordValue !== "") {
      const url =
        "http://127.0.0.1:8000/api/users/" +
        localStorage.userId +
        "?" +
        newUser;
      alert(url);
      return axios
        .put(url, {
          headers: {
            Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYWJiZGZiZDBjZGY1M2Q2NGQ5ZGU1ZTI3N2RjOWQ0NGM2ZTA3MzQ1ZDJiNDNlODM0MWFjMmU2NjM0MDAwNDk4ZmY2MjQwYjA5OTAzYjY2YWQiLCJpYXQiOjE1ODk4MzE4ODMsIm5iZiI6MTU4OTgzMTg4MywiZXhwIjoxNjIxMzY3ODgzLCJzdWIiOiIxNiIsInNjb3BlcyI6W119.eW_l3Tl8gJANBDkTCQT2ES-klhWqrPTHhEd2HvYg_u4zcRmxmX_aUP806dbbpqaB56OezdhmyX-VGP04cPA3usAwdPvx8f7GTCyTz_CE3R0-PTRaZd-LUXU3B7hUC9yfilXuFskdoG-locaUkuNwQb0SlY_2ZTWL3utxkYbtAg-H1yDgqZyNN0KsgT-L9Hf_4wWpkaCDzQIVLsr72Eg44X7AU5Z0aoRUAMk-XsqVzAtI92YcbS4jpyZ9zzkWOWJ9Oh2VOcAQjn1JhJPgakm8RdDQTGs-yQP9NXjOHrKLhVrjEl3DAhYyy1c3HHWUgLz-NJ0HXg3kx9P5Munlnza_4Uv--0kX74q_lZrez9WeCdodVagooKOrrS8s5CSaXHFVJ26eKPmwfx6jncSfaVmISYoc0M5Ix7GuvxgrR-0-S0LUHLyfhD5ac2p_vbFAQZn27E4RxjXdxuzqvD3pt7ZLVWrNTIJACyHc8lqYSWwM4s8wdx3YTXopbcKQsVRF8UI0hEO0e7kaFf5yDuYEpBWHighfOqa73BouQyC-Yq6VeiE9dmOmmZI7C9vw2XSvOK1fbEcxNjdTzW0sOqRpgtXvCi7UHB-dYtKRQmsuNT-O0kfzRb2ctDwHbFHZipot4W4cxQRa42s6q-qRZvIIscjBndpYN-slLXCATU6-aH3KWmE`,
          },
        })
        .then((res) => {
          console.log("User modified");
          return res;
        })
        .catch((err) => {
          console.log(err);
          alert(err);
        });
    } else {
      alert("Passwords don't match!");
    }
  };

  const handleHobbyChange = (e) => {
    setHobbyValue(e.target.value + " " + hobbyValue);
    console.log(hobbyValue);
  };

  const handleEmailChange = (e) => {
    setEmailValue(e.target.value);
    console.log(emailValue);
  };

  const handlePasswordChange = (e) => {
    setPasswordValue(e.target.value);
    console.log(passwordValue);
  };

  const handlePasswordcChange = (e) => {
    setPasswordcValue(e.target.value);
    console.log(passwordcValue);
  };
  const handleUsernameChange = (e) => {
    setUsernameValue(e.target.value);
    console.log(usernameValue);
  };
  const handleAvatarChange = (e) => {
    setAvatar(e.target.value);
    console.log(avatarValue);
  };

  useEffect(() => {
    // Start it off by assuming the component is still mounted
    let mounted = true;

    const loadData = async () => {
      const response = await getUser();
      // We have a response, but let's first check if component is still mounted
      if (mounted) {
        console.log(response.data.data);
        console.log(localStorage.userToken);
        localStorage.setItem("uname", response.data.data.name);
        localStorage.setItem("uemail", response.data.data.email);
        localStorage.setItem("uhobbies", response.data.data.hobbies);
        localStorage.setItem("uimage", response.data.data.image);
        localStorage.setItem("uid", response.data.data.id);
      }
    };
    loadData();
    return () => {
      // When cleanup is called, toggle the mounted variable to false
      mounted = false;
    };
  }, [url]);

  const BootstrapInput = withStyles((theme) => ({
    root: {
      "label + &": {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: 4,
      position: "relative",
      backgroundColor: theme.palette.background.paper,
      border: "1px solid #ced4da",
      fontSize: 16,
      padding: "10px 26px 10px 12px",
      transition: theme.transitions.create(["border-color", "box-shadow"]),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(","),
      "&:focus": {
        borderRadius: 4,
        borderColor: "#80bdff",
        boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
      },
    },
  }))(InputBase);
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar
          className={classes.avatar}
          src={localStorage.getItem("uimage")}
          className={classes.large}
        />

        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                placeholder={localStorage.getItem("uname")}
                name="username"
                label={t("auth.username")}
                type="text"
                id="username"
                autoComplete="username"
                onChange={handleUsernameChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                placeholder={localStorage.getItem("uemail")}
                id="email"
                label={t("auth.email")}
                name="email"
                autoComplete="email"
                onChange={handleEmailChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label={t("auth.password")}
                type="text"
                id="password"
                autoComplete="password"
                onChange={handlePasswordChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label={t("auth.passwordc")}
                type="text"
                id="password"
                autoComplete="password"
                onChange={handlePasswordcChange}
              />
            </Grid>
            <Grid item xs={12}>
              <label>
                {t("profile.hobby") + localStorage.getItem("uhobbies")}
              </label>
            </Grid>
            <Grid item xs={12}>
              <FormControl className={classes.margin}>
                <InputLabel htmlFor="demo-customized-select-native">
                  {t("auth.hobbies")}
                </InputLabel>
                <NativeSelect
                  id="demo-customized-select-native"
                  //value={age}
                  onChange={handleHobbyChange}
                  input={<BootstrapInput />}
                >
                  <option aria-label="None" value="" />
                  <option value={"Sport"}>{t("auth.hobby1")}</option>
                  <option value={"Tech"}>{t("auth.hobby2")}</option>
                  <option value={"Cooking"}>{t("auth.hobby3")}</option>
                </NativeSelect>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <input
                accept="image/*"
                className={classes.input}
                style={{ display: "none" }}
                id="raised-button-file"
                multiple
                type="file"
                onChange={handleAvatarChange}
              />
              <label htmlFor="raised-button-file">
                <Button
                  color="primary"
                  component="span"
                  className={classes.button}
                >
                  {t("auth.image")}
                </Button>
              </label>{" "}
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleChangeProfile}
          >
            {t("profile.modify")}
          </Button>

          <Grid container justify="flex-end">
            <Grid item></Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
export default withNamespaces()(UserProfile);
